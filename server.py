
import socket
import threading
import os

HOST = '128.141.94.150'  # Standard loopback interface address (localhost)
PORT = 65431        # Port to listen on (non-privileged ports are > 1023)
i = 0

def send_msg(conn):  
    while(1):
        send_msg = input() # User input here
        if (send_msg == 'quit' ):
            print('Connection to client closed')
            os._exit(0)
        
        if (send_msg != ''): # Makes sure is something typed before processing. 
            #Convert each letter send_msg an ASCII and saves it in an array.
            convert_to_ascii = [ord(c) for c in send_msg]
            
            #Append each element to bytearray
            my_bytes = bytearray()
            for x in convert_to_ascii:
                my_bytes.append(x)
            
            #sends data to client
            conn.send(my_bytes)

def recv_msg(conn):
    while(1):
        data = conn.recv(1024)
        print("Client: " + data.decode("utf-8"))

## main ##
if __name__ == "__main__":
        
        # creates socket object
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        s.bind((HOST, PORT))
        s.listen()
        print('Connecting to IP: {} Port: {}'.format(HOST,PORT))
        
        #seeking for the connection to be etablished
        conn, addr = s.accept()

        conn.send("Welcome to the server!".encode())
        print('Succes: Connection etablished')
        print("Send message to client or type 'quit' to exit")
        
        # creating threads
        send_msg_tread = threading.Thread(target=send_msg, args=(conn,))
        recv_msg_tread = threading.Thread(target=recv_msg, args=(conn,))

        #starting threads
        send_msg_tread.start()
        recv_msg_tread.start()