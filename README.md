# TCP Server Client implementation between Python and CPP
## Description

This code illustrates the establishment of a TCP socket for communication between a Python server and a C++ client. 
It is developed as a foundation for a later project between the remote PC and EMP.

After the connection between the server and client is established, the code allows the user to send and receive text messages between the client and server. For managing the send and receive instructions simultaneously, I used multithreading to avoid blocking commands.

# Usage

1. Make sure that the IP address of your server matches the IP address given in the code
2. Run server.py on one device, and cpp_client.cpp on another device
3. If everything went well, you will be able to write messages between the two devices
